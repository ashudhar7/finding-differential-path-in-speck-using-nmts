
# coding: utf-8

# In[1]:

def mask_1(a,b,wshift):
    s1=0
    var=0
    while(s1==0):
        if(wshift==0):
            return 0
        a1=(a<<(wshift))&mask
        b1=(b<<(wshift))&mask
        s1=a1+b1
        if(s1==0):
            var=var+1
        wshift=wshift-1
        mask1=(mask<<var)&mask
    return mask1


# ## calculate weight when input alpha beta and output gamma are given(fixed)

# In[2]:

def weight1(alpha1,beta1,gamma1):  
    x1= (alpha1 << 1)& mask
    y1= (beta1  << 1)& mask
    z1= (gamma1  << 1)& mask
    not_x=(x1^mask)
    eq=((not_x)^y1)&(not_x^z1)
    s=eq&(alpha1^beta1^gamma1^(y1))
    if(s==0):        
        h=bin((~eq)& mask)
        wt=(h[1:].count("1"))
        return wt
    else:
        return 0


# In[3]:

#alpha=49281
#beta=32895
#gamma=16382
#wt=weight1(alpha,beta,gamma)
#print (wt)


# ## function to calculate weight for any gamma when inputs alpha beta are given(fixed) 

# In[4]:

import random
def weight(alpha1,beta1):  
    mask1=mask_1(alpha1,beta1,wshift)  
    #random decision for output value gamma1
    gamma1= (random.randint(0,mask))&mask1
    if(alpha1==0 and beta1==0 and gamma1==0):
        return 0,1,gamma1
    x1= (alpha1 << 1)& mask
    y1= (beta1  << 1)& mask
    z1= (gamma1  << 1)& mask
    not_x=(x1^mask)
    eq=((not_x)^y1)&(not_x^z1)
    s=eq&(alpha1^beta1^gamma1^(y1))
    if(s==0):        
        h=bin((~eq)& mask)
        wt=(h[1:].count("1"))
        #return weight and random output for valid differential
        #print hex(gamma1),wt
        return wt,s,gamma1
    else:
        return 0,1,gamma1
    


# ## left and right circular shift function used by speck implementation

# In[5]:

#right circular shifts
def ROR(x,r):
    x = ((x << (SPECK_TYPE - r)) + (x >> r)) & mask
    return x

#left circular shifts
def ROL(x,r):
    x = ((x >> (SPECK_TYPE - r)) + (x << r)) & mask
    return x


# ## implementation of speck cipher

# In[6]:

def find_diff_path(alph,bet,gam,st0,st1,SPECK_ROUNDS, n):
    lp=1             
    temp_wt=0
    tempwt_list=[]
    tempdec_list=[]
    for i in range (0,SPECK_ROUNDS):
        chk=1
        tempdec_list.append(st0)   
        tempdec_list.append(st1) 
        st1=ROR(st1,alpha)
        for ch in range(0,len(alph)):
            if(alph[ch] == st0 and bet[ch]==st1):
                wt=weight1(alph[ch],bet[ch],gam[ch])
                op=gam[ch]
                chk=0
                break
        if(chk!=0):
            while(lp!=0):
                wt,lp,op= weight(st0,st1)
            lp=1
        tempdec_list.append(op) 
        #weight of differential probability updated in decision list
        tempwt_list.append(wt)
        temp_wt= temp_wt+tempwt_list[i]
        #update state with new valid output differential
        st1=op
        st0=ROL(st0,beta); st0=st0^st1
    n=n+1
    return tempwt_list, tempdec_list, temp_wt, n


# ## nested monte carlo function applying on speck

# In[7]:

def find_best_path(alph,bet,gam,st0,st1,SPECK_ROUNDS,wt_above,best_wt):
    #using n as index value for list
    n=0
    #print st0,st1
    for r in range(SPECK_ROUNDS,0,-1):
        tempwt_list, tempdec_list, temp_wt, n = find_diff_path(alph,bet,gam,st0,st1,r, n)
        if((temp_wt+wt_above) < best_wt):
            best_wt= temp_wt+wt_above
            for i,j in zip(range(3*(n-1),66), range(len(tempdec_list))):
                dec_list[i]= tempdec_list[j]
            for i,j in zip(range(n-1,22), range(0,len(tempwt_list))):
                wt_list[i]= tempwt_list[j]
            #for round (round-1) passing the states and weight
        if(n<22):
            st0=dec_list[3*(n+1)-3]
            st1=dec_list[3*(n+1)-2]
        wt_above= wt_above+wt_list[n-1]
    return best_wt  


# # main function 

# In[ ]:

#alpha beta are for left and right circular shift     
alpha, beta = 8,3
SPECK_ROUNDS=9
SPECK_TYPE=32
mask = 2 ** 32 - 1
wshift=31
wt_above=0
best_wt=99999
wt_list=[0]*22
dec_list=[0]*66
s=9999

#alph,bet,gam are files for highway values
alph,bet,gam = [],[],[]
with open('alph.txt') as inputfile:
    for line in inputfile:
        alph.append(int(line.strip()))
        
with open('bet.txt') as inputfile:
    for line in inputfile:
        bet.append(int(line.strip()))
        
with open('gam.txt') as inputfile:
    for line in inputfile:
        gam.append(int(line.strip()))
      
#nested loop to run number of time
while(best_wt>57):
    #number of elements in list of alpha or beta or gama
    ran= random.randint(0,3951387)
    st1=ROL(bet[ran],alpha)
    st0=alph[ran]
    best_wt=find_best_path(alph,bet,gam,st0,st1,SPECK_ROUNDS,wt_above,best_wt)
    if(best_wt<s):
             s=best_wt
             print (best_wt)
    
             

print ("Dec list is:")
for i in range(0,SPECK_ROUNDS):    
    print ("Starting input of %i round and weight:" %i,hex(dec_list[3*i+1]), hex(dec_list[3*i]),hex(dec_list[3*i+2]),(wt_list[i]))
       
print ("Best weight is:", best_wt) 
    


# In[ ]:



