#function to calculate weight of differential probability of addition modulo with given input alpha, beta and randomly created output gamma

#to create another mask for output to reduce running time of program
def mask_1(a,b,wshift):
        s1=0
        var=0
        while(s1==0):
                a1=(a<<(wshift))&mask
                b1=(b<<(wshift))&mask
                s1=a1+b1
                if(s1==0):
                    var=var+1
                wshift=wshift-1
                mask1=(mask<<var)&mask
        return mask1
    

import random
def weight(alpha1,beta1):  
    mask1=mask_1(alpha1,beta1,wshift)  
    #random decision for output value gamma1
    gamma1= (random.randint(0,mask))&mask1
    x1= (alpha1 << 1)& mask
    y1= (beta1  << 1)& mask
    z1= (gamma1  << 1)& mask
    not_x=(x1^mask)
    eq=((not_x)^y1)&(not_x^z1)
    s=eq&(alpha1^beta1^gamma1^(y1))
    if(s==0):        
        h=bin((~eq)& mask)
        wt=(h[1:].count("1"))
        #return weight and random output for valid differential
        #print hex(gamma1),wt
        return wt,s,gamma1
    else:
        return 0,1,gamma1
    
#right circular shifts
def ROR(x,r):
    x = ((x << (SPECK_TYPE - r)) + (x >> r)) & mask
    return x

#left circular shifts
def ROL(x,r):
    x = ((x >> (SPECK_TYPE - r)) + (x << r)) & mask
    return x

def find_diff_path(st0,st1,SPECK_ROUNDS, n):
            lp=1             
            temp_wt=0
            tempwt_list=[]
            tempdec_list=[]
            for i in range (0,SPECK_ROUNDS):
                tempdec_list.append(st0)   
                tempdec_list.append(st1) 
                st1=ROR(st1,alpha)
                #condition to check random output decision with valid differential
                while(lp!=0):            
                    wt,lp,op= weight(st0,st1)
                lp=1
                #addition of random output decision in decision list, if output hold with some differential probability
                tempdec_list.append(op) 
                #weight of differential probability updated in decision list
                tempwt_list.append(wt)
                temp_wt= temp_wt+tempwt_list[i]
                #update state with new valid output differential
                st1=op
                st0=ROL(st0,beta); st0=st0^st1
            n=n+1
            return tempwt_list, tempdec_list, temp_wt, n

def find_best_path(st0,st1, SPECK_ROUNDS, wt_above, best_wt):
    #using n as index value for list
    n=0
    #print st0,st1
    for r in range(SPECK_ROUNDS,0,-1):
            tempwt_list, tempdec_list, temp_wt, n = find_diff_path(st0,st1,r, n)
            if((temp_wt+wt_above) < best_wt):
                 best_wt= temp_wt+wt_above
                 for i,j in zip(xrange(3*(n-1),66), xrange(len(tempdec_list))):
                     dec_list[i]= tempdec_list[j]
                 for i,j in zip(xrange(n-1,22), xrange(0,len(tempwt_list))):
                     wt_list[i]= tempwt_list[j]
            #for round (round-1) passing the states and weight
            if(n<22):
                st0=dec_list[3*(n+1)-3]
                st1=dec_list[3*(n+1)-2]
            wt_above= wt_above+wt_list[n-1]
    return best_wt       

#alpha beta are for left and right circular shift    
alpha, beta = 8,3
SPECK_ROUNDS=4
SPECK_TYPE=24
mask = 2 ** 24 - 1
wshift=23
wt_above=0
best_wt=99999
wt_list=[0]*22
dec_list=[0]*66
s=9999
#initial state (random created initial plaintext)
#st0=1, st1=0 
#st0=(random.randint(1,mask))
#st1=(random.randint(1,mask))
st0=0x8124A0
st1=0x802080
c=0
#nested loop to run number of time
while(best_wt>26):
    best_wt=find_best_path(st0,st1,SPECK_ROUNDS, wt_above, best_wt )
    if(best_wt<s):
        s=best_wt
        print best_wt 

print "Dec list is:"
for i in range(0,SPECK_ROUNDS):    
    print "Starting input of %i round and weight:" %i,hex(dec_list[3*i+1]), hex(dec_list[3*i]),hex(dec_list[3*i+2]),(wt_list[i])
    
print    

print "Best weight is:", best_wt 
    
